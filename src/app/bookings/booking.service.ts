import { Injectable } from '@angular/core';
import { Booking } from './booking.model';

@Injectable({ providedIn: 'root' })
export class BookingService {
  private _bookings: Booking[] = [
    new Booking('b1', 'p1', 'Can Tho', 2, 'u1'),
  ];

  get bookings() {
    return [...this._bookings];
  }
}
