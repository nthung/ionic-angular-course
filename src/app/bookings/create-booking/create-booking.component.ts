import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { BookingDetail } from 'src/app/places/discover/place-detail/place-detail.page';
import { Place } from 'src/app/places/place.model';

@Component({
  selector: 'app-create-booking',
  templateUrl: './create-booking.component.html',
  styleUrls: ['./create-booking.component.scss'],
})
export class CreateBookingComponent implements OnInit {
  @Input() selectedPlace: Place;
  @Input() selectedMode: CreationBookingMode;
  @ViewChild('f') form: NgForm;

  preFillDateFrom: string;
  preFillDateTo: string;

  constructor(private modalController: ModalController) {}

  ngOnInit() {
    if (this.selectedMode === 'random') {
      const difference =
        this.selectedPlace.availableTo.getTime() -
        this.selectedPlace.availableFrom.getTime();
      const range = Math.ceil(difference / (24 * 3600 * 1_000)) - 2;
      const startDay = range - Math.round(Math.random() * range);
      this.preFillDateFrom = new Date(
        this.selectedPlace.availableFrom.getFullYear(),
        this.selectedPlace.availableFrom.getMonth(),
        this.selectedPlace.availableFrom.getDate() + startDay
      ).toISOString();

      this.preFillDateTo = new Date(
        this.selectedPlace.availableFrom.getFullYear(),
        this.selectedPlace.availableFrom.getMonth(),
        this.selectedPlace.availableFrom.getDate() + startDay + 2
      ).toISOString();
    }
  }

  onCancel() {
    this.modalController.dismiss(null, 'cancel');
  }

  onBookPlace() {
    if (!this.form.valid || !this.datesValid) {
      return;
    }

    const bookingData: BookingDetail = {
      firstName: this.form.value['first-name'],
      lastName: this.form.value['last-name'],
      numberOfGuests: +this.form.value['guest-number'],
      fromDate: this.form.value['from-date'],
      toDate: this.form.value['to-date'],
    };
    this.modalController.dismiss(bookingData, 'confirm');
  }

  get datesValid() {
    if (!this.form) {
      return false;
    }
    const fromDate = new Date(this.form.value['from-date']).getTime();
    const toDate = new Date(this.form.value['to-date']).getTime();
    return toDate > fromDate;
  }
}

export type CreationBookingMode = 'random' | 'specific';
