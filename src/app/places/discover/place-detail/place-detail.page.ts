import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  ActionSheetController,
  ModalController,
  NavController,
} from '@ionic/angular';
import {
  CreateBookingComponent,
  CreationBookingMode,
} from '../../../bookings/create-booking/create-booking.component';
import { Place } from '../../place.model';
import { PlacesService } from '../../places.service';

@Component({
  selector: 'app-place-detail',
  templateUrl: './place-detail.page.html',
  styleUrls: ['./place-detail.page.scss'],
})
export class PlaceDetailPage implements OnInit {
  place: Place;

  constructor(
    private navController: NavController,
    private route: ActivatedRoute,
    private placesService: PlacesService,
    private modalController: ModalController,
    private actionSheetController: ActionSheetController
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe((paramMap) => {
      if (!paramMap.has('placeId')) {
        this.navController.navigateBack('/places/tabs/discover');
        return;
      }
      const placeId = paramMap.get('placeId');
      this.place = this.placesService.getPlace(placeId);
    });
  }

  onBookPlace() {
    this.actionSheetController
      .create({
        backdropDismiss: true,
        header: 'Choose an Action',
        buttons: [
          {
            text: 'Choose Specific',
            handler: () => {
              this.openBookingModal('specific');
            },
          },
          {
            text: 'Choose Random',
            handler: () => {
              this.openBookingModal('random');
            },
          },
          {
            text: 'Cancel',
            role: 'cancel',
          },
        ],
      })
      .then((actionSheet) => {
        actionSheet.present();
      });
  }

  openBookingModal(mode: CreationBookingMode) {
    this.modalController
      .create({
        component: CreateBookingComponent,
        componentProps: {
          selectedPlace: this.place,
          selectedMode: mode,
        },
      })
      .then((modal) => {
        modal.present();
        return modal.onDidDismiss<BookingDetail>();
      })
      .then((modal) => {
        if (modal.role === 'confirm') {
          console.log('Booked');
        }
      });
  }
}

export interface BookingDetail {
  firstName: string;
  lastName: string;
  numberOfGuests: number;
  fromDate: string;
  toDate: string;
}
