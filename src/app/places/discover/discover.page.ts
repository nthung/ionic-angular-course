import { Component, OnInit } from '@angular/core';
import { Place } from '../place.model';
import { PlacesService } from '../places.service';

@Component({
  selector: 'app-discover',
  templateUrl: './discover.page.html',
  styleUrls: ['./discover.page.scss'],
})
export class DiscoverPage implements OnInit {
  loadedPlaces: Place[];
  listLoadedPlaces: Place[];

  constructor(private placesService: PlacesService) {}

  ngOnInit() {
    this.loadedPlaces = this.placesService.places;
    this.listLoadedPlaces = this.loadedPlaces.slice(1);
  }

  onSwitchPlaces(event: CustomEvent) {
    console.log(event.detail);
    if (event.detail.value === 'all') {
      this.listLoadedPlaces = this.loadedPlaces;
    } else {
      this.listLoadedPlaces = this.loadedPlaces.slice(1);
    }
  }
}
