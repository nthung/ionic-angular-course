import { Injectable } from '@angular/core';
import { Place } from './place.model';

@Injectable({
  providedIn: 'root',
})
export class PlacesService {
  private _places: Place[] = [
    new Place(
      'p1',
      'Can Tho',
      'Trung tam Dong bang song Cuu Long',
      'http://www.mrlinhadventure.com/UserFiles/image/VIETNAM%20HIGHLIGHTS/Can%20Tho/Can%20Tho%20overview.jpg',
      1000,
      new Date('2020-12-01'),
      new Date('2021-12-01')
    ),
    new Place(
      'p2',
      'Ha Noi',
      'Thu do cua Vietnam',
      'http://static.asiawebdirect.com/m/bangkok/portals/vietnam/homepage/hanoi/hanoi-districts/pagePropertiesImage/hanoi-old-quarter.jpg',
      8889,
      new Date('2020-12-01'),
      new Date('2021-12-01')
    ),
    new Place(
      'p3',
      'Vinh Ha Long',
      'Du lich tai Quang Ninh',
      'https://media.tacdn.com/media/attractions-splice-spp-674x446/06/73/3f/7b.jpg',
      5689,
      new Date('2020-12-01'),
      new Date('2021-12-01')
    ),
  ];

  get places() {
    return [...this._places];
  }

  getPlace(placeId: string) {
    return { ...this._places.find((place) => place.id === placeId) };
  }

  constructor() {}
}
