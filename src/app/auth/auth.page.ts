import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, IonInput, ToastController } from '@ionic/angular';

import { AuthService } from './auth.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.page.html',
  styleUrls: ['./auth.page.scss'],
})
export class AuthPage implements OnInit {
  isLoading = false;

  isLoginMode = true;

  constructor(
    private authService: AuthService,
    private router: Router,
    private loadingController: LoadingController,
    private toastController: ToastController
  ) {}

  ngOnInit() {}

  onLogin() {
    this.authService.login();
    this.isLoading = true;
    this.loadingController
      .create({
        backdropDismiss: false,
        message: 'Loading...',
      })
      .then((loadingElement) => {
        loadingElement.present();
        setTimeout(() => {
          this.isLoading = false;
          loadingElement.dismiss();
          this.router.navigateByUrl('/places/tabs/discover');
        }, 1500);
      });
    this.toastController
      .create({
        message: 'You need to enter email first',
        duration: 1000,
      })
      .then((toast) => {
        toast.present();
      });
  }

  onSubmit(form: NgForm) {
    if (!form.valid) {
      return;
    }
    const email = form.value.email;
    const password = form.value.password;
    console.log(email, password);

    if (this.isLoginMode) {

    } else {

    }
    form.reset();
  }

  onSwitchMode() {
    this.isLoginMode = !this.isLoginMode;
  }
}
